<?php
abstract class Hewan {
	public $nama;
	public $darah = 50;
	public $jumlahKaki;
	public $keahlian;
	
	
	public function atraksi($nama, $keahlian){
		echo $nama.' sedang '.$keahlian;
	}
}
 
trait Fight {
	public $attackPower;
	public $defencePower;
	
	public function serang($hewan_1){
		$this->diserang($hewan_1);
		echo $this->nama.' sedang menyerang '.$hewan_1->nama.'<br />';
		echo "sisa darah ".$this->darah;
		
	}
	
	public function diserang($hewan_1){
		echo $this->nama.' sedang diserang '.$hewan_1->nama.'<br />';
		$this->darah -= $this->attackPower/$hewan_1->defencePower;
		return $this->darah;
	}
}
 
class Elang extends Hewan {
	use Fight;
	
	public function getInfoHewan(){
		echo "Nama : ".$this->nama.'<br />';
		echo "Jumlah Kaki : ".$this->jumlahKaki.'<br />';
		echo "Keahlian : ".$this->keahlian.'<br />';
		echo "Attack Power : ".$this->attackPower.'<br />';
		echo "Deffence Power : ".$this->defencePower.'<br />';
	}
	
	
}
 
class Harimau extends Hewan{
	use Fight;
	
	public function getInfoHewan(){
		echo "Nama : ".$this->nama.'<br />';
		echo "Jumlah Kaki : ".$this->jumlahKaki.'<br />';
		echo "Keahlian : ".$this->keahlian.'<br />';
		echo "Attack Power : ".$this->attackPower.'<br />';
		echo "Deffence Power : ".$this->defencePower.'<br />';
	}
	
}

$elang = new Elang();
$elang->nama='Elang 1';
$elang->jumlahKaki=2;
$elang->keahlian='terbang tinggi';
$elang->attackPower=10;
$elang->defencePower=5;
$elang->getInfoHewan();

echo '<br /><br />';
$harimau = new Harimau();
$harimau->nama='Harimau 1';
$harimau->jumlahKaki=4;
$harimau->keahlian='lari cepat';
$harimau->attackPower=7;
$harimau->defencePower=8;
$harimau->getInfoHewan();

echo '<br /><br />';
echo '<b>Pertandingan dimulai 1</b><br />';
$elang->serang($harimau);
echo '<br /><br />';
echo '<b>Pertandingan dimulai 2</b><br />';
$harimau->serang($elang);