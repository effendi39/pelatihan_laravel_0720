<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['auth', 'admin'])->group(function(){
    Route::get('/admin', 'TestController@admin');
});


Route::get('/route-1', 'TestController@SuperAdmin')->middleware('admin:superadmin, admin');
Route::get('/route-2', 'TestController@admin')->middleware('admin:admin');
Route::get('/route-3', 'TestController@Guest')->name('home');