<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role='')
    {
        $userRole = $request->user();
 
        if($userRole && $userRole->count() > 0)
        {
            $userRole = $userRole->role;
            
            $checkRole = 0;
            if($userRole == $role && $role =='superadmin')
            {
                $checkRole = 1;
            }
            elseif($userRole == $role && $role == 'admin')
            {
                $checkRole = 1;
            }
            if($checkRole == 1)
                return $next($request);
            else
               return abort(401);
        }
        else
        {
            return redirect('login');
        }
    }
}
