<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class TestController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function test(){
        return 'berhasil masuk';
    }

    public function SuperAdmin(){
        return 'berhasil masuk Super Admin';
    }

    public function admin(){
        
        return 'Admin berhasil masuk';
    }

    public function Guest(){
        
        return 'Guest berhasil masuk';
    }
}
