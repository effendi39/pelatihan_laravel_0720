<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataPinjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pinjaman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal_peminjaman');
            $table->date('tanggal_batas_akhir_peminjaman');
            $table->date('tanggal_pengembalian');
            $table->string('nim');
            $table->string('kode_buku')->unique();
            $table->boolean('status_ontime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pinjaman');
    }
}
