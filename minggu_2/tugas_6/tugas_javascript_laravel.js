/*   Soal 1 */
var golden = () => {
	//function
    return () => {
         console.log("this is golden!!")
    }
}
golden();

/*   Soal 2 */
const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 



/*  Soal 3*/
let newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation)


/* Soal 4*/
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
//let combined = west.concat(east)
//Driver Code
//console.log(combined)

let combined = [...west, ...east];
console.log(combined)


/*Soal 5*/
const planet = "earth"
const view = "glass"
var before1 = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' + 'incididunt ut labore et dolore magna aliqua. Ut enim' + ' ad minim veniam'   // Driver Code console.log(before) 
const first = 'Lorem '
const second = 'dolor sit amet, consectetur adipiscing elit, '
const third = 'do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam'
const before = `${first} ${view} ${second} ${planet} ${third}`
console.log(before) 
